Feature: Game Details
  In order to know more about a game
  As a user
  I want to see game's detail

  @javascript
  Scenario: Happy path
    Given there are some games
    When I am on the games page
    And I click on the game's name
    Then I see the game details

