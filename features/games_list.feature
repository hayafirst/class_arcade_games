Feature: Games List
  In order to browse games
  As a user
  I want to see all the games in one page

  @javascript
  Scenario: Happy path
    Given there are some games
    When I am on the games page
    Then I see all games
