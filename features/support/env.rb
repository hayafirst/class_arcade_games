ENV['RAILS_ENV'] = 'feature'
require './config/environment'

require 'rspec-rails'
require 'rspec/expectations'
require 'capybara/poltergeist'
require 'spinach/capybara'

Dir[Rails.root.join("features/steps/shared_steps/**/*.rb")].each {|f| require f}

require 'database_cleaner'
DatabaseCleaner.strategy = :truncation

Capybara.javascript_driver = :poltergeist
Capybara.default_wait_time   = 30

Spinach.hooks.before_scenario do
  DatabaseCleaner.clean
end

Spinach.hooks.on_tag('javascript') do |scenario, step_definitions|
  ::Capybara.current_driver = ::Capybara.javascript_driver
end

Spinach.config.save_and_open_page_on_failure = true
