class Spinach::Features::GameDetails < Spinach::FeatureSteps
  include SharedSteps::ToGamesPage

  step "I click on the game's name" do
    click_link game.name
  end

  step 'I see the game details' do
    within("dl") do
      expect(page).to have_content game.note
    end
  end
end
