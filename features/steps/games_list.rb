class Spinach::Features::GamesList < Spinach::FeatureSteps
  include SharedSteps::ToGamesPage
  step 'I see all games' do
    within("tbody") do
      expect(page).to have_content("Game 1")
      expect(page).to have_content("M1")
    end
  end
end
