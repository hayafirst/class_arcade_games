module SharedSteps
  module ToGamesPage
    def self.included(mod)
      mod.send :attr_reader, :game
      mod.step "there are some games" do
        @game = Game.create!(
          name:          "Game 1",
          year:          1988,
          manufacturer:  "M1",
          active:        "1",
          notes:         "Some note"
        )
      end

      mod.step 'I am on the games page' do
        visit "/"
      end
    end
  end
end
