module Forms
  module Dsls
    module DelegateValidations
      def delegate_validations(*args)
        options = args.pop
        fields = args
        model_name = options[:to]
        fields.each do |form_field, model_field|
          model_field = options[:on] || form_field
          const_set( "#{form_field.to_s.classify}On#{model_name.to_s.classify}Validator", validator_class(model_name, model_field, options))
          validates form_field, "#{form_field}_on_#{model_name}".to_sym => true
        end
      end

      def validator_class(model_name, field, options)
        Class.new(ActiveModel::EachValidator) do
          cattr_accessor :model_name, :model_field, :options
          self.model_name = model_name
          self.model_field = field
          self.options = options
          def validate_each(record, attribute, value)
            model = record.send(self.model_name)
            model.send("#{self.class.model_field}=", value)
            model.valid?
            if !model.errors[self.class.model_field].blank?
              if options[:message]
                record.errors.add attribute, options[:message]
              else
                model.errors[self.class.model_field].each do |error_msg|
                  record.errors.add attribute, error_msg
                end
              end
            end
            model.reload unless model.new_record?
          end
        end
      end
    end
  end
end
