module Forms
  class Like < Base
    class << self
      def attributes
        [:game_id]
      end
    end

    attr_reader :game_id, :request

    delegate_validations :ip, to: :model

    def initialize(request, params)
      @request = request
      @game_id = params[:game_id]
    end

    def save
      if valid?
        model.save
      end
    end

    private

    def model
      @model ||= ::Like.new ip: ip, game_id: game_id, liked_at: Time.zone.now
    end

    def ip
      request.remote_ip
    end

  end
end
