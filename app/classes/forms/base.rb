module Forms
  class Base
    include ActiveModel::Validations
    include ActiveModel::Conversion
    extend ActiveModel::Naming
    attr_reader :model

    def self.model_name
      ::ActiveModel::Name.new(self, nil, self.name.demodulize)
    end

    extend Dsls::DelegateValidations

    def persisted?
      false
    end
  end
end
