module Utilities
  class ImageSize
    attr_reader :image_path
    def initialize(image_path)
      @image_path = image_path
    end

    def fit_to_width(max_size)
      size = FastImage.size(image_path)
      return size if size[0] <= max_size
      scale = size[0]/max_size.to_f
      size.map{|s| (s/scale).to_i }
    end
  end
end
