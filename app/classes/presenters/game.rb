module Presenters
  class Game
    include ModelPresenter::Base
    include ActionView::Helpers::AssetUrlHelper
    forward_from_model :name, :manufacturer, :id, :notes

    def active
      model.active? ? "Yes" : "No"
    end

    def photo
      "#{ENV["GAME_IMAGE_URL"]}#{model.photo_file_name}" if model.photo_file_name.present?
    end

    def photo_width
      return nil unless photo
      photo_size[0]
    end

    def photo_height
      return nil unless photo
      photo_size[1]
    end

    def liked_by?(remote_ip)
      model.likes.count(ip: remote_ip) > 0
    end

    def liked_count
      model.likes.count
    end

    private

    def photo_size
      @size ||= Utilities::ImageSize.new("#{Rails.root}/public#{photo}").fit_to_width(200)
    end
  end
end
