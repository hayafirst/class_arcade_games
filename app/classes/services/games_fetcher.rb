module Services
  module GamesFetcher
    class << self
      def list
        Game.active.order("year").limit(25)
      end
    end
  end
end
