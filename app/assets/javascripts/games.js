window.games = (function() {
  var element = null;
  var showDialog = function() {
    $(element).popModal({
      html: function(callback) {
        $.ajax({
          url:$(element).attr('href')
        }).done(function(content){
          callback(content);
        });
      }
    });
  };

  var likeThis = function() {
    $.ajax({
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      },
      url:$(element).attr('href'),
      method: "POST",
      success: function(content){
        $(".popModal").html(content);
      },
      error: function(jqXHR, textStatus, errorThrown){
        $("#error").html(jqXHR.responseJSON.message);
      }
    });
  };

  return {
    getReady: function() {
      $("tbody").on("click", "a.game-show", function(e){
        e.preventDefault();
        element = $(this);
        showDialog();
      });
      $("body").on("click", "a.like", function(e){
        e.preventDefault();
        element = $(this);
        likeThis();
      });
    }
  };
})();
