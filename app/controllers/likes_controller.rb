class LikesController < ApplicationController
  def create
    form = Forms::Like.new request, form_params
    if form.save
      @game = Presenters::Game.new(Game.find(params[:game_id]))
      render partial: 'games/show'
    else
      render json: { message: form.errors.full_messages }, status: 422
    end
  end

  private

  def form_params
    params.permit(*Forms::Like.attributes)
  end
end
