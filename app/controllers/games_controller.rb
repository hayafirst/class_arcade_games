class GamesController < ApplicationController
  def index
    games = Services::GamesFetcher.list
    @games = games.map{|g| Presenters::Game.new g }
  end

  def show
    game = Game.find params[:id]
    @game = Presenters::Game.new game
    render partial: 'show'
  end
end
