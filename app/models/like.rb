class Like < ActiveRecord::Base
  belongs_to :game
  validates :ip, uniqueness: { scope: :game_id }
end
