class Game < ActiveRecord::Base
  has_many :likes
  validates :name, presence: true, uniqueness: true
  scope :active, -> { where(active: true) }
end
