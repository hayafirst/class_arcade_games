FactoryGirl.define do
  factory :game do
    sequence(:name) {|n| "Game #{n}" }
    active true

    trait :inactive do
      active false
    end
  end
end
