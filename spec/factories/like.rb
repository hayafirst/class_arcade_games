FactoryGirl.define do
  factory :like do
    sequence(:ip) {|n| "111.111.11.#{n}" }
    game
  end
end
