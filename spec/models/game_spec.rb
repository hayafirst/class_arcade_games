require 'spec_helper'

RSpec.describe Game do
  let(:object_under_test) { FactoryGirl.build :game }
  describe "validations" do
    it_requires :name

    context "name uniqueness" do
      let(:duplicate) { FactoryGirl.build :game, name: object_under_test.name }

      it "is unique among the name" do
        object_under_test.save!
        expect(duplicate).not_to be_valid
        expect(duplicate.errors[:name]).to eq([ "has already been taken" ])
      end
    end
  end

  describe ".active" do
    let!(:inactive) { FactoryGirl.create :game, :inactive }
    let!(:active) { FactoryGirl.create :game }

    it "returns only active games" do
      expect(Game.active.all).to eq([ active ])
    end
  end
end
