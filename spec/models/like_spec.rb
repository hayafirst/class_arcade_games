require 'spec_helper'

RSpec.describe Like do
  let(:like) { FactoryGirl.build :like }
  describe "validations" do
    describe "ip uniqueness" do
      let!(:existing_like) { FactoryGirl.create :like }
      let(:new_like) { FactoryGirl.build :like, ip: existing_like.ip, game: game }

      context "same game" do
        let(:game) { existing_like.game }
        it "is invalid" do
          expect(new_like).to be_invalid
        end

        it "includes ip error" do
          new_like.valid?
          expect(new_like.errors[:ip]).to eq(["has already been taken"])
        end
      end

      context "different game" do
        let(:game) { FactoryGirl.create :game }
        it "is valid" do
          expect(new_like).to be_valid
        end
      end
    end
  end
end
