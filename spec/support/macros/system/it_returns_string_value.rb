module RspecMacros
  module System
    def it_returns_string_value(value)
      it "returns '#{value}'" do
        expect(result).to eq(value)
      end
    end

    def it_returns_nil
      it "returns nil" do
        expect(result).to be_nil
      end
    end
  end
end


