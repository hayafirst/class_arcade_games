module RspecMacros
  module System
    def load_tasks
      before(:each) do
        Rake::Task.clear
        ClassArcadeGames::Application.load_tasks
      end
    end
  end
end

