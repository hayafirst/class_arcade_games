module RspecMacros
  module System
    def it_requires(*attrs)
      options = attrs.pop if attrs.last.is_a?(Hash)
      attrs.each do |attr|
        it "requires the #{attr.inspect}" do
          allow(object_under_test).to receive(attr).and_return ""
          expect(object_under_test).to be_invalid
          expect(object_under_test.errors[attr]).to eq([options.try(:[], :message) || "can't be blank"])
        end
      end
    end
  end
end

