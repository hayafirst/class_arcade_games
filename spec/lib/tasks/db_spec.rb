require 'spec_helper'
require 'rake'

RSpec.describe 'db:import_from_csv', type: :model do
  load_tasks
  let(:file_path) { "#{Rails.root}/spec/fixtures/games.csv" }
  let(:task) { Rake::Task['db:import_from_csv'] }
  let(:game) { Game.where(name: 'BurgerTime').first }
  let(:battle_zone) { Game.where(name: 'Battlezone').first }
  let(:inactive_game) { Game.where(name: 'Berzerk').first }

  before(:each) do
    task.invoke(file_path)
  end

  it "imports all records" do
    expect(Game.count).to eq(3)
  end

  it "imports data correctly" do
    expect(game.manufacturer).to eq('Data East (Japan) / Bally Midway (US)')
  end

  context "with image" do
    it "imports photo_file_name" do
      expect(game.photo_file_name).to eq('burgertime.jpg')
    end
  end

  context "without image" do
    it "sets photo_file_name to nil" do
      expect(battle_zone.photo_file_name).to be_nil
    end
  end

  context "with is_active 0" do
    it "sets the game to be inactive" do
      expect(inactive_game).not_to be_active
    end
  end

  context "with is_active 1" do
    it "sets the game to be active" do
      expect(game).to be_active
    end
  end
end
