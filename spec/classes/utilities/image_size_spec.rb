require 'spec_helper'

RSpec.describe Utilities::ImageSize do
  let(:image_size) { described_class.new 'file_name' }
  describe "#fit_to_width" do
    let(:result) { image_size.fit_to_width(max_width) }
    before(:each) do
      allow(FastImage).to receive(:size).and_return([309, 560])
    end

    context "width requirement is smaller than the actual" do
      let(:max_width) { 100 }

      it "scales down" do
        expect(result).to eq([100, 181])
      end
    end

    context "width requirement is bigger than the actual" do
      let(:max_width) { 400 }

      it "returns the original size" do
        expect(result).to eq([309, 560])
      end
    end
  end
end
