require 'spec_helper'

RSpec.describe Presenters::Game do
  let(:presenter){ described_class.new model }
  let(:activeness) { true }
  let(:photo_file_name) { "image.jpg" }
  let(:messages) do
    {
      name: "a name",
      manufacturer: "manufacturer",
      id: "aaa",
      notes: "notes",
      active?: activeness,
      photo_file_name: photo_file_name
    }
  end
  let(:model) { double :model, messages }

  forward_from_model_attributes :name, :manufacturer, :id, :notes

  describe "#active" do
    let(:result) { presenter.active }
    context "the model is active" do
      let(:activeness) { true }
      it_returns_string_value "Yes"
    end

    context "the model is inactive" do
      let(:activeness) { false }
      it_returns_string_value "No"
    end
  end

  describe "#photo" do
    let(:result) { presenter.photo }
    context "with a photo_file_name" do
      it_returns_string_value "/games_images/image.jpg"
    end

    context "without a photo_file_name" do
      let(:photo_file_name) { "" }
      it_returns_nil
    end
  end

  class << self
    def it_returns_calculated_size_when_available(dimension_name)
      context "available" do
        let(:photo_file_name) { 'image.jpg' }
        let(:size) { [ 200, 345 ] }
        let(:image_size) { double :image_size, fit_to_width: size }
        let(:image_path) { Rails.root.join("public", "games_images", "image.jpg").to_s }
        let(:dimension) { dimension_name == :width ? 0 : 1 }
        before(:each) do
          allow(Utilities::ImageSize).to receive(:new).and_return image_size
        end

        it "initializes a Utilities::ImageSize instance" do
          expect(Utilities::ImageSize).to receive(:new).with(image_path).and_return image_size
          result
        end

        it "asks the image_size for the right size" do
          expect(image_size).to receive(:fit_to_width).with(200).and_return size
          expect(result).to eq(size[dimension])
        end
      end

      context "unavailable" do
        let(:photo_file_name) { '' }
        it_returns_nil
      end
    end
  end

  describe "#photo_width" do
    let(:result) { presenter.photo_width }

    it_returns_calculated_size_when_available :width
  end

  describe "#photo_height" do
    let(:result) { presenter.photo_height }
    it_returns_calculated_size_when_available :height
  end

  describe "#liked_by?" do
    let(:likes) { double :likes_dataset, count: count_result }
    let(:remote_ip) { "111.222.333.444" }
    let(:result) { presenter.liked_by?(remote_ip) }
    let(:count_result) { 0 }
    before(:each) do
      allow(model).to receive(:likes).and_return likes
    end

    it "tries to finds a like" do
      expect(likes).to receive(:count).with(ip: remote_ip).and_return 0
      result
    end

    context "finds a like with the same IP" do
      let(:count_result) { 1 }
      it "returns true" do
        expect(presenter).to be_liked_by(remote_ip)
      end
    end

    context "doesn't find a like with the same IP" do
      let(:count_result) { 0 }
      it "returns true" do
        expect(presenter).not_to be_liked_by(remote_ip)
      end
    end
  end

end
