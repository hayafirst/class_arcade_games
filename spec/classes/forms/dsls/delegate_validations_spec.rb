require 'spec_helper'

RSpec.describe Forms::Dsls::DelegateValidations do
  let(:klass) {
    Class.new do
      extend Forms::Dsls::DelegateValidations
      include ActiveModel::Validations

      attr_accessor :attr1, :attr2, :attr3
      attr_accessor :model1, :model2
    end
  }

  let(:model_class1) {
    Class.new do
      include ActiveModel::Validations
      attr_accessor :model_attr1, :attr3
      attr_reader :reload
      validates :model_attr1, :attr3, presence: true
      validates :model_attr1, numericality: true

      def self.name
        "MyName"
      end

      def new_record?
        false
      end
    end
  }

  let(:model_class2) {
    Class.new do
      include ActiveModel::Validations
      attr_accessor :attr2
      attr_reader :reload
      validates :attr2, presence: true

      def self.name
        "MyName2"
      end

      def new_record?
        false
      end
    end
  }

  let(:object) { klass.new }
  let(:model1) { model_class1.new }
  let(:model2) { model_class2.new }

  before(:each) do
    klass.send :delegate_validations, :attr1, to: :model1, on: :model_attr1
    klass.send :delegate_validations, :attr2, to: :model2
    object.attr1 = 12
    object.attr2 = "attr2"
    object.model1 = model1
    object.model2 = model2
  end

  class << self
    def it_is_invalid_with_nil_attr(attr)
      describe "#{attr} is nil" do
        before(:each) do
          object.send "#{attr}=", nil
        end

        it "is invalid" do
          expect(object).to be_invalid
        end

        it "has an error on #{attr}" do
          object.valid?
          expect(object.errors[ attr ]).to include( "can't be blank" )
        end
      end

    end
  end

  it_is_invalid_with_nil_attr :attr1
  it_is_invalid_with_nil_attr :attr2

  it "reloads model to clear the values" do
    expect(model1).to receive( :reload )
    expect(model2).to receive( :reload )
    object.valid?
  end

  context "with non numeric attr1" do
    it "is invalid" do
      object.attr1 = "aaa"
      object.valid?
      expect(object.errors[ :attr1 ]).to include( "is not a number" )
    end
  end

  context "with custom message" do
    it "uses the custom message" do
      object.attr1 = "aaa"
      object.valid?
      expect(object.errors[ :attr1 ]).to include( "is not a number" )
    end
  end

  context "both attrs are valid" do
    it "is valid" do
      klass.send :delegate_validations, :attr3, to: :model1, message: "crazy"
      object.attr3 = nil
      expect(object).to be_invalid
      expect(object.errors[ :attr3 ]).to include( "crazy" )
    end
  end
end
