require 'spec_helper'

RSpec.describe Forms::Like do
  let(:request) { double :request, remote_ip: "remote_ip" }
  let(:params) { { game_id: "111" } }
  let(:form) { described_class.new request, params }

  describe "#save" do
    let(:result) { form.save }
    let(:like) { double :like_model, save: true }
    before(:each) do
      allow(form).to receive(:valid?).and_return validness
    end

    context "when the form is invalid" do
      let(:validness) { false }

      it "returns false" do
        expect(result).to be_falsey
      end

      it "doesn't create Like" do
        expect(like).not_to receive(:save)
        result
      end
    end

    context "when the form is valid" do
      let(:validness) { true }
      let(:time) { "2016-09-09 11:18 AM".to_time }

      it "returns true" do
        expect(result).to be_truthy
      end

      it "creates Like" do
        Timecop.freeze(time) do
          expect(::Like).to receive(:new).with(ip: "remote_ip", game_id: "111", liked_at: time).and_return like
          expect(like).to receive(:save)
          result
        end
      end
    end
  end
end
