require 'spec_helper'

RSpec.describe Services::GamesFetcher do
  describe ".list" do
    let(:action) { described_class.list }
    let(:limited_dataset) { double :limited_dataset }
    let(:order_dataset) { double :order_dataset, limit: limited_dataset }
    let(:active_dataset) { double :active_dataset, order: order_dataset }

    before(:each) do
      allow(Game).to receive(:active).and_return active_dataset
    end

    it "fetches active games only" do
      expect(Game).to receive(:active).and_return active_dataset
      action
    end

    it "orders by year of Publication" do
      expect(active_dataset).to receive(:order).with("year").and_return order_dataset
      action
    end

    it "fetches 25 records" do
      expect(order_dataset).to receive(:limit).with(25).and_return []
      action
    end
  end
end
