require 'spec_helper'

RSpec.describe LikesController do
  describe "POST#create" do
    let(:save_result) { false }
    let(:errors) { double :errors, full_messages: "a message" }
    let(:form) { double :form, save: save_result, errors: errors }
    let(:action) { post :create, params}
    let(:params) { { game_id: "aaa" } }
    let(:form_params) { { game_id: "aaa" } }

    before(:each) do
      allow(Forms::Like).to receive(:new).and_return form
    end

    it "uses a form for creating a like" do
      expect(Forms::Like).to receive(:new).with(request, form_params).and_return form
      action
    end

    it "saves the form" do
      expect(form).to receive(:save)
      action
    end

    context "the save fails" do
      let(:save_result) { false }

      it "renders a message back" do
        action
        expect(JSON.parse(response.body, symbolize_names: true)).to eq(message: "a message")
      end

      it "returns status 422" do
        action
        expect(response.status).to eq(422)
      end
    end

    context "the save succeeds" do
      let(:presenter) { double :presenter }
      let(:game) { double :game }
      let(:save_result) { true }

      before(:each) do
        allow(Presenters::Game).to receive(:new).and_return presenter
        allow(Game).to receive(:find).and_return game
      end

      it "assigns a presenter" do
        action
        expect(assigns(:game)).to eq(presenter)
      end

      it "renders the game show" do
        action
        expect(response).to render_template("games/_show")
      end
    end
  end
end
