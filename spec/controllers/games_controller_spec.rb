require 'spec_helper'

RSpec.describe GamesController do
  let(:game) { double :game }
  let(:presenter) { double :presenter }
  describe "GET#index" do
    let(:action) { get :index }
    let(:games) { [ game ] }
    before(:each) do
      allow(Services::GamesFetcher).to receive(:list).and_return games
    end

    it "initializes a service" do
      expect(Services::GamesFetcher).to receive(:list).and_return games
      action
    end

    it "initializes the game list" do
      expect(Presenters::Game).to receive(:new).and_return presenter
      action
    end
  end

  describe "GET#show" do
    let(:action) { get :show, id: "123" }

    before(:each) do
      allow(Game).to receive(:find).and_return game
    end

    it "finds the game" do
      expect(Game).to receive(:find).with("123").and_return game
      action
    end

    it "initializes a presenter" do
      expect(Presenters::Game).to receive(:new).and_return presenter
      action
    end

    it "renders the show partial" do
      action
      expect(response).to render_template(:partial => '_show')
    end

  end
end
