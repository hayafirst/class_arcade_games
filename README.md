# README

This is a typical Rails application so I am going to skip most common task for setting up a rails app. Rather I would like to focus on specific ones

## Setup

The app uses dotenv and foreman. To run the app

```bash
$ cp .env.example .env/development
```

and change the value accordingly

To start server:

```bash
$ foreman start -e .env/development
```

The server is setup to run on port `3001`

## Data import

```bash
$ bin/rake db:import_from_csv["/your/path/to/games.csv"]
```

## Running test

```bash
$ cp .env.example .env/test
```

and change the values accordingly

```bash
$ bin/rspec
```

## Running feature tests

```bash
$ cp .env/test .env/feature
```

and change the values accordingly

```bash
$ bin/spinach
```

Using bin version enables spring to take control so the second run will be faster.

## Coding Comments

Even not absolutely needed, I implemented services, forms and presenters in this app just to show some ideas we talked about in the interview.
