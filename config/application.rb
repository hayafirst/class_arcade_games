require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
if Gem::Specification.find_all_by_name('dotenv').any?
  require 'dotenv'
  Dotenv.load ".env/#{ Rails.env || :development }"
end

module ClassArcadeGames
  class Application < Rails::Application
    config.active_record.raise_in_transactional_callbacks = true
  end
end
