Rails.application.routes.draw do
  resources :games, only: [ :index, :show ] do
    resource :likes, only: [ :create ]
  end
  root 'games#index'
end
