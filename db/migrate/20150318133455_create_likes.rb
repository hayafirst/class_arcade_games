class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.datetime :liked_at
      t.string :ip
      t.references :game, index: true
    end
  end
end
