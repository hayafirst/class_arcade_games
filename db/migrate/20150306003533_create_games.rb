class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :name, null: false
      t.integer :year
      t.string :manufacturer
      t.string :notes
      t.boolean :active
      t.string :photo_file_name
    end
    add_index :games, :name, unique: true
  end
end
