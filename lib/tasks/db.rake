require 'csv'

namespace :db do
  desc "Import from a given CSV file"
  task :import_from_csv, [ :file_name ] => :environment do |t, args|
    attributes_array = []
    CSV.foreach( args["file_name" ], { headers: true } ) do |row|
      attributes = row.to_hash.symbolize_keys
      activeness = BooleanConverter.new(attributes[:is_active]).to_boolean
      attributes.merge!(active: activeness).delete(:is_active)
      attributes.merge!(photo_file_name: image_maps[attributes[:name]])
      attributes_array << attributes
    end
    Game.create! attributes_array
  end

  def image_maps
    {
      'Berzerk' => "BERZERK.jpg",
      'Centipede' => "centipede.jpg",
      'Donkey Kong Junior' => "donkey_kong.jpg",
      'Frogger'=> "frogger.jpg",
      'Asteroids'=> "asteroids.jpg",
      'Defender'=> "defender.jpg",
      "Dragon's Lair"=> "dragons_lair.jpg",
      'Galaga'=> "galaga.jpg",
      'BurgerTime'=> "burgertime.jpg",
      'Dig Dug'=> "dig_dug.png",
      'Elevator Action'=> "elevator_action.jpg"
    }
  end

  class BooleanConverter
    attr_reader :value
    def initialize(value)
      @value = value
    end

    def to_boolean
      value.to_s == '1' ? true : false
    end
  end
end
